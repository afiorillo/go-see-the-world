package main

import (
	log "github.com/sirupsen/logrus"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

// views

func main() {
	log.Info("starting server...")
	r := router.New()
	r.GET("/", Healthcheck)
	r.GET("/crawl", Crawl)

	if err := fasthttp.ListenAndServe(":8080", r.Handler); err != nil {
		log.Panicf("error in ListenAndServe: %v", err)
	}
}
