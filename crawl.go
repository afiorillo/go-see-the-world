package main

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/gocolly/colly"
	"github.com/tidwall/buntdb"
)

type Crawler struct {
	collector *colly.Collector
	db        *buntdb.DB
}

func NewCrawler() *Crawler {
	return &Crawler{
		collector: colly.NewCollector(
			colly.MaxDepth(1),
			// colly.AllowedDomains("hackerspaces.org", "wiki.hackerspaces.org"),
		),
	}
}

func (c *Crawler) openDatabase() {
	log.Info("opening database")
	db, err := buntdb.Open(":memory:")
	if err != nil {
		log.Fatal(err)
	}
	c.db = db
}

func (c *Crawler) closeDatabase() {
	log.Info("closing database")
	c.db.Close()
}

func (c *Crawler) GivenURL(url string, depth int) string {
	c.openDatabase()
	defer c.closeDatabase()

	if depth > 0 {
		c.collector.MaxDepth = depth
	}

	c.collector.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		log.WithFields(log.Fields{
			"link_text": e.Text,
			"link_href": link,
		}).Info()

		// check if this has already been visited
		err := c.db.View(func(tx *buntdb.Tx) error {
			val, err := tx.Get(link)
			if err != nil {
				return err
			}
			log.Infof("%s in buntdb: %s", link, val)
			return nil
		})
		if err == buntdb.ErrNotFound {
			e.Request.Visit(link)
		} else if err != nil {
			log.Error(err)
		} else {
			log.Info("skipping ", link)
		}

	})

	c.collector.OnRequest(func(r *colly.Request) {
		c.db.Update(func(tx *buntdb.Tx) error {
			_, _, err := tx.Set(r.URL.EscapedPath(), "bar", nil)
			return err
		})

		// log.WithFields(log.Fields{
		// 	"request": fmt.Sprintf("%+v", r),
		// 	"url":     r.URL.EscapedPath(),
		// }).Info("request done")
	})

	start := time.Now()
	c.collector.Visit(url)
	return fmt.Sprintf("took %.2f seconds", time.Now().Sub(start).Seconds())
}
