package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"github.com/valyala/fasthttp"
)

func Healthcheck(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(ctx, "ok")
	log.Println(string(ctx.Method()), string(ctx.Path()))
}

func Crawl(ctx *fasthttp.RequestCtx) {
	args := ctx.QueryArgs()
	root_url := string(args.Peek("root_url"))
	depth := args.GetUintOrZero("depth")
	log.WithFields(log.Fields{
		"root_url": root_url,
		"depth":    depth,
	}).Info("")

	crawler := NewCrawler()
	status := crawler.GivenURL(root_url, depth)
	// log.Info("done crawling")
	fmt.Fprint(ctx, status)
}
